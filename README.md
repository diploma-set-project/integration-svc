# science-practice-integration-svc

Integration Service

Azure integration to get result of analyzing of computer vision

To run project:

```bash

node . AZURE_API_HOST AZURE_API_KEY PATH_TO_CAPTCHA_FOLDER PATH_TO_PATH_JSON MONGO_BASE_URL

```

Service will recognize date and push it into mongo database.