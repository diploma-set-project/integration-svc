const sleep = require('util').promisify(setTimeout);
const path = require('path');
const computerVisionRecognizeText = require('./integration');
const mapAndSave = require('./mapping');
const mongoose = require("mongoose");

async function main() {
    const captchaFolder = process.argv[4];
    const pathJsonFile = process.argv[5];

    await mongoose.connect(process.argv[6]).then(_ => console.log('Mongo Connected!!!'));
    await writeResultFromAzureByPathJson(captchaFolder, pathJsonFile)
}

async function writeResultFromAzureByPathJson(captchaFolder, pathJsonFile) {

    const file = require(pathJsonFile)

    for (const imageInfo of file) {
        const pathToImage = path.join(captchaFolder, imageInfo.path.replace('captcha', '').replace('./', ''));
        console.log(pathToImage, ' - image is in progress...');
        const recognizedText = await computerVisionRecognizeText(pathToImage);

        await mapAndSave(imageInfo, recognizedText);
        console.log(pathToImage, ' - image has finished!');
        await sleep(5000)
    }

}

main();