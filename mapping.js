const mongoose = require('mongoose');
const {Schema} = require("mongoose");

const ImageAnalyze = mongoose.model('ImageAnalyzePreDiploma', new Schema({
    name: String,
    text: String,
    textFromAzure: String,
    length: Number,
    lengthFromAzure: Number,
    level: Number,
    color: String,
    bg: String,
    isEqual: Boolean,
    isEqualIgnoreCase: Boolean,
    isEqualLength: Boolean,
    equalityByLetters: Array
}, {
    timestamps: true
}));

module.exports = async function(imageInfo, recognizedText) {
    const imageAnalyze = new ImageAnalyze(toMap(imageInfo, recognizedText))
    await imageAnalyze.save()
}

function toMap(imageInfo, recognizedText) {
    return {
        name: imageInfo.path,
        text: imageInfo.text,
        textFromAzure: recognizedText,
        length: imageInfo.text.length,
        lengthFromAzure: recognizedText.length,
        level: findOutLevel(imageInfo.path),
        color: imageInfo.color,
        bg: imageInfo.bg,
        isEqual: Boolean(imageInfo.text === recognizedText),
        isEqualIgnoreCase: Boolean(imageInfo.text.toLowerCase() === recognizedText.toLowerCase()),
        isEqualLength: Boolean(imageInfo.text.length === recognizedText.length),
        equalityByLetters: getDetailedEquality(imageInfo.text, recognizedText)
    }
}

function findOutLevel(path) {
    let levelInPath = Number(path.split('/')[2]);
    let level = 10;
    for (let i = 3; i < 13; i++) {
        if (i === levelInPath) {
            return level;
        }
        level--;
    }
}

function getDetailedEquality(originalText, recognizedText) {
    let primaryText = originalText.split('');
    let secondText = recognizedText.split('');

    if (recognizedText.length > primaryText) {
        primaryText = secondText;
        secondText = originalText.split('');
    }

    return primaryText.map((letter, index) => {

        if (!secondText[index]) {
            return false;
        }

        return letter.toLowerCase() === secondText[index].toLowerCase();
    });
}



