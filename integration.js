const fs = require('fs');
const sleep = require('util').promisify(setTimeout);
const ComputerVisionClient = require('@azure/cognitiveservices-computervision').ComputerVisionClient;
const ApiKeyCredentials = require('@azure/ms-rest-js').ApiKeyCredentials;

const key = process.argv[3];
const endpoint = process.argv[2];

const computerVisionClient = new ComputerVisionClient(
    new ApiKeyCredentials({ inHeader: { 'Ocp-Apim-Subscription-Key': key } }), endpoint);

const STATUS_SUCCEEDED = "succeeded";
const STATUS_FAILED = "failed";

async function readTextFromURL(client, path) {
    const image = await readFileToBlob(path);
    let result = await client.readInStream(image);
    const operation = result.operationLocation.split('/').slice(-1)[0];

    while (result.status !== STATUS_SUCCEEDED) {
        await sleep(1000);
        result = await client.getReadResult(operation);
    }
    return result.analyzeResult.readResults;
}

async function readFileToBlob(pathToFile) {
    return new Promise(resolve => {
        fs.readFile(pathToFile, (error, data) => {
            if(error) {
                throw error;
            }
            const buffer = Buffer.from(data);
            resolve(Uint8Array.from(buffer).buffer);
        });
    });
}

function getFromResponse(readResults) {
    for (const page in readResults) {
        const result = readResults[page];
        if (result.lines.length) {
            let textResult = ''
            for (const line of result.lines) {
                textResult = line.words.map(w => w.text).join(' ');
            }
            return textResult;
        } else {
            return '';
        }
    }
}

module.exports = async (filename) => {
    return new Promise(resolve => {
        readTextFromURL(computerVisionClient, filename)
            .then((result) => resolve(getFromResponse(result)));
    })
}
